const express = require('express');
const router = express.Router();
const ObjectID = require('mongodb').ObjectID;
const {handleError, handleSuccess, handleFindResponse, handleInsertResponse, handleDeleteResponse} = require('../../utilities');

/**
 * query string params - criteria
 */
router.get('/users/:id/ideas', (req, res)=>{
    const db = req.app.locals.db;
    const collection = db.collection('ideas');
    const userid = req.params.id;
    const query_params = req.query;
    const criteria = {};

    if ( !userid ) {
        handleError(res, "ERROR: Unable to get user info. Please login again");
        return;
    }

    switch (query_params.criteria) {
        case "all":
            criteria['owner'] = userid
            break;

        case "favorites":
            criteria['favoriteBy'] = userid;
            break;

        case "likes": 
            criteria['likedBy'] = userid;
            break;

        default:
            criteria['owner'] = userid
            break;
    }

    collection.find(criteria).toArray()
    .then(db_response=>{
        const success_message = "SUCCESS: Ideas fetched successfully";
        const failure_message = "ERROR: Failed to fetch ideas";
        handleFindResponse(res, db_response, success_message, failure_message );
    })
    .catch(err=>{
        handleError(res, "ERROR: Unknown error occured", err);
    });
});

router.post('/users/:id/ideas', (req, res)=>{
    const db = req.app.locals.db;
    const collection = db.collection('ideas');
    const userid = req.params.id;
    const {title, description, submission_type} = req.body;

    if (!userid){
        handleError(res, "ERROR: Unable to get user info. Please login again");
        return;
    }

    const collection_fields = {
        favorites: 0,
        favoriteBy: [],
        likes: 0,
        likedBy: [],
        dislikes: 0,
        dislikedBy: [],
        views: 0,
        viewedBy: [],
        matchIds: [],
        submissionType: submission_type,
        isDeleted: false,
        topics: [],
        keywords: [],
        title: title,
        description: description,
        owner: userid
    }

    collection.insertOne({ ...collection_fields })
    .then(db_response => {
        const success_message = (submission_type === "published") ? "SUCCESS: Idea published successfully" : "SUCCESS: Draft added successfully";
        const failure_message = (submission_type === "published") ? "ERROR: Failed to published idea" : "ERROR: Failed to add a draft";
        handleInsertResponse(res, db_response, success_message, failure_message);
    })
    .catch(err=>{
        handleError(res, "ERROR: An unknown error occured", err);
    });
});

router.delete('/users/:id/ideas/:idea_id', (req, res)=>{
    const db = req.app.locals.db;
    const collection = db.collection('ideas');
    const user_id = req.params.id;
    const idea_id = req.params.idea_id;

    if ( !user_id ) {
        handleError(res, "ERROR: Unable to get user info. Please login again");
        return;
    }
    if ( !idea_id ) {
        handleError(res, "ERROR: No idea provided");
    }

    collection.deleteOne({
        _id: ObjectID(idea_id)
    })
    .then(db_response=>{
        const success_message = "SUCCESS: Idea deleted successfully";
        const failure_message = "ERROR: Failed to delete the idea"
        handleDeleteResponse(res, db_response, success_message, failure_message);
    })
    .catch(err=>{
        handleError(res, "ERROR: Unknown error occured", err);
    })
})

module.exports = router;