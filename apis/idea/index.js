const express = require('express');
const router = express.Router();
const ObjectID = require('mongodb').ObjectID;
const {handleError, handleUpdateResponse, handleFindResponse} = require('../../utilities');

/**
 * GET: /ideas
 * Fetch all available Ideas in db
 */
router.get('/ideas', (req, res)=>{
    //TODO handle an edge case when db connection is broken
    const db = req.app.locals.db;
    const collection = db.collection('ideas');
    collection.find({}).toArray()
    .then((db_response)=>{
        const success_message = "SUCCESS: Ideas fetched successfully";
        const failure_message = "ERROR: Ideas not found";
        handleFindResponse(res, db_response, success_message, failure_message )
    })
    .catch(err=>{
        handleError(res, "ERROR: Unknown error occured", err);
    })
});

/**
 * GET: /idea/:id
 * Fetch an idea by its id
 */
router.get('/ideas/:id', (req, res)=>{
    const db = req.app.locals.db;
    const collection = db.collection('ideas');
    const idea_id = req.params.id;
    
    collection.findOne({ _id: new ObjectID(idea_id) }, {})
    .then((db_response)=>{
        const success_message = "SUCCESS: Idea fetched successfully";
        const failure_message = "ERROR: Idea not found";
        handleFindResponse(res, db_response, success_message, failure_message );
    })
    .catch(err=>{
        handleError(res, "ERROR: Unknown error occured", err);
    })
});

router.post('/ideas/:id/favorites', (req, res)=>{
    const db = req.app.locals.db;
    const collection = db.collection('ideas');
    const user_id = req.body.user_id;
    const idea_id = req.params.id;
    if (!user_id){
        handleError(res, "ERROR: Unable to get user info. Please login again");
        return
    }
    collection.updateOne(
        { _id: new ObjectID(idea_id) },
        { 
            $push: {favoriteBy: user_id },
            $inc: {favorites: 1}
        }
    )
    .then((db_response)=>{
        const success_message = "SUCCESS: Idea added to your favorites!";
        const failure_message = "ERROR: Operation could not be completed";
        handleUpdateResponse(res, db_response, success_message, failure_message );
    })
    .catch(err=>{
        handleError(res, "ERROR: Unknown error occured", err);
    })
});

router.delete('/ideas/:id/favorites', (req, res)=>{
    const db = req.app.locals.db;
    const collection = db.collection('ideas');
    const user_id = req.body.user_id;
    const idea_id = req.params.id;
    if (!user_id){
        handleError(res, "ERROR: Unable to get user info. Please login again");
        return;
    }
    collection.updateOne(
        { _id: new ObjectID(idea_id) },
        { 
            $inc: { favorites: -1 },
            $pull: { favoriteBy: user_id }
        }
    )
    .then((db_response)=>{
        const success_message = "SUCCESS: Idea removed from your favorites";
        const failure_message = "ERROR: Operation could not be completed";
        handleUpdateResponse(res, db_response, success_message, failure_message );
    })
    .catch(err=>{
        handleError(res, "ERROR: Unknown error occured", err);
    })
})

router.post('/ideas/:id/likes', (req, res)=>{
    const db = req.app.locals.db;
    const collection = db.collection('ideas');
    const user_id = req.body.user_id;
    const idea_id = req.params.id;
    if (!user_id){
        handleError(res, "ERROR: Unable to get user info. Please login again");
    }
    collection.updateOne(
        { _id: new ObjectID(idea_id) },
        { 
            $push: {likedBy: user_id },
            $inc: {likes: 1}
        }
    )
    .then((db_response)=>{
        const success_message = "SUCCESS: Idea added to your Likes collection!";
        const failure_message = "ERROR: Operation could not be completed";
        handleUpdateResponse(res, db_response, success_message, failure_message );
    })
    .catch(err=>{
        handleError(res, "ERROR: Unknown error occured", err);
    })
});

router.delete('/ideas/:id/likes', (req, res)=>{
    const db = req.app.locals.db;
    const collection = db.collection('ideas');
    const user_id = req.body.user_id;
    const idea_id = req.params.id;
    if (!user_id){
        handleError(res, "ERROR: Unable to get user info. Please login again");
    }
    collection.updateOne(
        { _id: new ObjectID(idea_id) },
        { 
            $inc: { likes: -1 },
            $pull: { likedBy: user_id }
        }
    )
    .then((db_response)=>{
        const success_message = "SUCCESS: Idea removed from your Likes collection!";
        const failure_message = "ERROR: Operation could not be completed";
        handleUpdateResponse(res, db_response, success_message, failure_message );
    })
    .catch(err=>{
        handleError(res, "ERROR: Unknown error occured", err);
    })
})

router.post('/ideas/:id/dislikes', (req, res)=>{
    const db = req.app.locals.db;
    const collection = db.collection('ideas');
    const user_id = req.body.user_id;
    const idea_id = req.params.id;
    if (!user_id){
        handleError(res, "ERROR: Unable to get user info. Please login again");
    }
    collection.updateOne(
        { _id: new ObjectID(idea_id) },
        { 
            $push: {dislikedBy: user_id },
            $inc: {dislikes: 1}
        }
    )
    .then((db_response)=>{
        const success_message = "SUCCESS: Idea disliked";
        const failure_message = "ERROR: Operation could not be completed";
        handleUpdateResponse(res, db_response, success_message, failure_message );
    })
    .catch(err=>{
        handleError(res, "ERROR: Unknown error occured", err);
    })
});

router.delete('/ideas/:id/dislikes', (req, res)=>{
    const db = req.app.locals.db;
    const collection = db.collection('ideas');
    const user_id = req.body.user_id;
    const idea_id = req.params.id;
    if (!user_id){
        handleError(res, "ERROR: Unable to get user info. Please login again");
    }
    collection.updateOne(
        { _id: new ObjectID(idea_id) },
        { 
            $inc: { dislikes: -1 },
            $pull: { dislikedBy: user_id }
        }
    )
    .then((db_response)=>{
        const success_message = "SUCCESS: Idea removed from your Disliked collection!";
        const failure_message = "ERROR: Operation could not be completed";
        handleUpdateResponse(res, db_response, success_message, failure_message );
    })
    .catch(err=>{
        handleError(res, "ERROR: Unknown error occured", err);
    })
})

module.exports = router;