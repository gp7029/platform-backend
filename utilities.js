const ObjectID = require('mongodb').ObjectID;

const handleError = (res, message, err=null) => {
    if (!err) {
        res.status(500).send({
            payload: {},
            message
        });   
    } else {
        res.status(500).send({
            payload: err,
            message
        });
    }
}

const handleSuccess = (res, message, payload={} ) => {
    res.status(200).send({
        payload,
        message
    })
}

const handleUpdateResponse = (res, db_response, success_message="", failure_message="") => {
    if (!db_response){
        handleError(res, "ERROR: No response from database");
    } else {
        if (db_response.result && db_response.result.ok === 1){
            handleSuccess(res, success_message, db_response.result);
        } else {
            handleError(res, failure_message);
        }
    }
}

const handleFindResponse = (res, db_response, success_message="", failure_message="") => {
    if (!db_response){
        handleError(res, failure_message);
    } else {
        handleSuccess(res, success_message, db_response);
    }
}

const handleInsertResponse = (res, db_response, success_message="", failure_message="") => {
    if (db_response.insertedCount === 1 && db_response.insertedId) {
        const object_id = db_response.insertedId;
        const timestamp = ObjectID(object_id).getTimestamp();
        const payload = {
            object_id,
            timestamp
        }
        handleSuccess(res, success_message, payload);
    } else {
        handleError(res, failure_message);
    }
}

const handleDeleteResponse = (res, db_response, success_message="", failure_message="") => {
    if (!db_response){
        handleError(res, "ERROR: No response from database");
    } else {
        if (db_response.result && db_response.result.n === 1){
            handleSuccess(res, success_message, db_response.result);
        } else {
            handleError(res, failure_message);
        }
    }
}

module.exports = {
    handleSuccess,
    handleError,
    handleUpdateResponse,
    handleFindResponse,
    handleInsertResponse,
    handleDeleteResponse
}