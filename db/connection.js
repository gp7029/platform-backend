const mongoclient = require('mongodb').MongoClient;
const config = require('../config.json');

module.exports = (app) => {
    mongoclient.connect(config.url, {useNewUrlParser: true, useUnifiedTopology: true}, (err, client) => {
        if (err) {
            console.error(err);
        }
    
        const db = client.db(config.db_name);
        app.locals.db = db;
    });   
}